#include "avr/io.h"
#include "util/delay.h"


/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
// We connected a led to the Pin PB4 of ATTiny.
// give it a name:
int led = PB4;

// the loop routine runs over and over again forever:
int main() {
  
  // PB4 en sortie
  DDRB |= (1<<led);
  
  // PB4 en entree
  //DDRB &= ~(1<<led);
  

  while(1){
	PORTB |= (1<<led); // sortie haute 
	_delay_ms(500);
	PORTB &= ~(1<<led); // sortie basse 
	_delay_ms(500);
  }

  return 0;
}




