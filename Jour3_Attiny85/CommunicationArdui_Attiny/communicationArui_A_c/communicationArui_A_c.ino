/*
  Communication protocole
  - Serial (UART) with the computer
  - Serial with an ATTiny85
 */

// Digital pin used
int ASK = 3;
int CLK = 13;
int DAT = 12;

// The setup routine runs once when you press reset:
void setup() {
  
  // Initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  
  // Initialize pin
  pinMode(ASK, INPUT_PULLUP);   // Entree
  pinMode(CLK, OUTPUT);         // Sortie
  pinMode(DAT, INPUT_PULLUP);   // Entree
}

// The loop routine runs over and over again forever:
void loop() {
  
  // ASK ==> 1 pulse 1ms
  pinMode(ASK, OUTPUT);
  digitalWrite(ASK, LOW); 
  delay(1);
  pinMode(ASK, INPUT_PULLUP);
  delay(1);
  
  // Read the input pin:
  int askState = digitalRead(ASK);
  
  // If the Attiny answers to the signal ==> we send the 8-bits pulsation
  if(!askState){
      
      char res = 0;
      
      for(int i=0 ; i<8 ;i++){
        
        //Front montant
        digitalWrite(CLK, HIGH);
        delay(50);
        
        // Listen for the bit on DAT
        int eltMess = digitalRead(DAT);
      
        // Letter reconstitution bit by bit
        if(eltMess){
         res += 1 << i; 
        }
        
        delay(50);
        
        //Front descandant
        digitalWrite(CLK, LOW);
        delay(100);
      }  
      
      // Print of the letters on the computer    
      Serial.println(res);
  }

  delay(10);        // delay in between reads for stability
}



