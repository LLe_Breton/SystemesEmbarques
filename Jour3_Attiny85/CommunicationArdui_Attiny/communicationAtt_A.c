#include "avr/io.h"
#include "util/delay.h"

/*
  Communication with arduino 
  SLAVE ! 
 */
 
// Connections
int led = PB4;
int ASK = PB3;
int CLK = PB2;
int DAT = PB1;

// Message to transfer
char message[] = "Hello World !";

// Variable to read the message
int i = 0;

int main() {
  
  // Pin sortie
  DDRB |= (1<<led);
  DDRB |= (1<<DAT);
  
  // Pin entree
  DDRB &= ~(1<<ASK);
  DDRB &= ~(1<<CLK);

  // Working loop
  while(1){
	
	// Gestion ASK 
	if(!(PINB & (1<<ASK))){

		DDRB |= (1<<ASK);             // mode sortie
		PORTB &= ~(1<<ASK);           // sortie basse
		_delay_ms(10); 
		DDRB &= ~(1<<ASK);            // mode entree

		// For each letter of the message, we send each of its bits on one pulsation
		char octet = message[i];     // letter
		int nbpuls=0;                // following of the pulsation
		int statePulse = -1;         // state of the Pulse

        // Pulsations
		while(nbpuls<8)
		{
			if(statePulse != (PINB & (1<<CLK))){

				// Adaptation of the pulse status
				statePulse = (PINB & (1<<CLK));
				
				if(statePulse){
					PORTB |= (1<<led);
					
					// Transmission of one bit
					if(octet & (1<<nbpuls)){
						PORTB |= (1<<DAT);
					}
					else{
						PORTB &= ~(1<<DAT);		
					}	
				}
				else{
					PORTB &= ~(1<<led);
					nbpuls++;
				}
			}	
		}

		// If the message is over, we restart it. Else we go to the next letter
		if (i > sizeof(message)){
			i = 0;
		}
		else{
			i++;
		}
		
		// We make sur that the led is off at the end of the pulsations
		PORTB &= ~(1<<led);
	}
	_delay_ms(1); 	
  }
  return 0;
}

