#include <stdio.h>
#include <limits.h>  // Tests taille variables
#include <stdlib.h>  // Allocation memoire

int main(){

// ===== Exercice 1 : Hello World

/*
	printf("Hello World \n");
*/

// ===== Exercice 2 : Taille de variables

/*  
    int a = 5;
    long b = 6;
    long long c = 2;
    char d = 127;
    float e = 2.0;
    double f = 5.0;
 
    printf("Taille d'un double : %lu octets \n", sizeof(f));
    printf("Taille d'un float : %lu octets \n", sizeof(e));
    printf("Taille d'un char : %lu octets \n", sizeof(d));
    printf("Taille d'un long long : %lu octets \n", sizeof(c));
    printf("Taille d'un long : %lu octets \n", sizeof(b));
    printf("Taille d'un int : %lu octets \n", sizeof(a));
*/    

// ===== Exercice 3 : Limites des variables 

/*
   printf("The number of bits in a byte %d\n", CHAR_BIT);

   printf("The minimum value of SIGNED CHAR = %d\n", SCHAR_MIN);
   printf("The maximum value of SIGNED CHAR = %d\n", SCHAR_MAX);
   printf("The maximum value of UNSIGNED CHAR = %d\n", UCHAR_MAX);

   printf("The minimum value of SHORT INT = %d\n", SHRT_MIN);
   printf("The maximum value of SHORT INT = %d\n", SHRT_MAX); 

   printf("The minimum value of INT = %d\n", INT_MIN);
   printf("The maximum value of INT = %d\n", INT_MAX);

   printf("The minimum value of CHAR = %d\n", CHAR_MIN);
   printf("The maximum value of CHAR = %d\n", CHAR_MAX);

   printf("The minimum value of LONG = %ld\n", LONG_MIN);
   printf("The maximum value of LONG = %ld\n", LONG_MAX);
 */

// ===== Exercice 4 : Limite de la division (int et double ...)

/*
    double div = 5/9;
    printf ("%g \n", div);

    double div2 = 5/9.0; //9.0 est un double
    printf ("%g \n", div2);
*/
   
// ===== Exercice 5 : Tests sur les pointeurs

/*
    int a = 5;
    int* b = &a;

    *b = 6;

    printf("Valeur de la variable a : %d \n", a);
    printf("Adresse de la variable a : %p et %p \n", &a, b);
*/   

// ===== Exercice 6 : Allocation memoire

/*
    int* po = malloc(8*sizeof(int)); // On alloue
    po[1] = 5;// On ecrit 5 dans la deuxieme case ATTENTION ==> sans * ! 

    printf("Valeur de la variable : %d \n", *(po + 1));

    // equivalent à 
    *(po + 1) = 13;

    printf("Valeur de la variable : %d \n", *(po + 1));
*/

// ===== Exercice 7 : passage par valeur/reference

/*
    void arithmetic(int* m, int* d, int* som, int* sou, int a, int b){

        *m = a*b;
        *d = a/b;
        *som = a+b;
        *sou = a-b;

    }

    int m,d,som,sou; // On ne déclare pas les pointeurs

    arithmetic(&m,&d,&som,&sou, 10,5);

    printf("Resultats mutliplication : %d \n", m);
    printf("Resultats division : %d \n", d);
    printf("Resultats somme : %d \n", som);
    printf("Resultats soustraction : %d \n", sou);
*/    

// ===== Exercice 8 : concatener des char en int

    unsigned char C1 = 3;
    unsigned char C2 = 6;
    unsigned char C3 = 0xeb;
    unsigned char C4 = 0b00111111;
    int c;
    int d;

    c = c | C1;
    c = c << 8;
    c = c | C2;
    c = c << 8;
    c = c | C3;
    c = c << 8;
    c = c | C4;

    d = (((d | C1)<<8 | C2)<<8 | C3)<<8 | C4 ; 
    
    printf("test c : %i \n", c);
    printf("test d : %i \n", d);

    return 0;
}
