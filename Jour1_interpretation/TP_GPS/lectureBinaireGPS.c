#include <stdlib.h>
#include <stdio.h>

// ==================== FONCTIONS ======================= 

// Fonction Extrcation Heure Minute Seconde
void displayTime(int *tim, unsigned char hexaIntH,unsigned char hexaIntM,unsigned char hexaIntS){
    tim[0] = hexaIntH;
    tim[1] = hexaIntM;
    tim[2] = hexaIntS;
}

// Fonction Exctraction Jour
void displayDay(int* day, unsigned char hexaInt){
    *day = hexaInt;
}

// Fonction Exctraction Mois
void displayMonth(int* month, unsigned char hexaInt){
    *month = hexaInt;
}

// Fonction Extrcation Annee
void displayYear(int* year, unsigned char hexaInt1, unsigned char hexaInt2){
    *year = hexaInt2 <<8 | hexaInt1; 
}

// Fonction checksum algorithme de fletcher - test de 8 bits
void checkAB(unsigned char* CK_A, unsigned char* CK_B, unsigned char hexa){
	*CK_A += hexa;
	if(*CK_A > 255){
		*CK_A -= 255;
	}
	*CK_B += *CK_A;
	if(*CK_B > 255){
		*CK_B -= 255;
	}
}


// ==================== MAIN ======================= 
int main(){

// ----------------------------------------------------
    
    // Initialisation des variables à enregistrer
    int tim[3];
    int day;
    int month;
    int year;

// ----------------------------------------------------
    
    // Test fonctions qui lisent le doc
    //displayTime(tim,0x12,0x00,0x0d);
    //displayDay(&day,0x01);
    //displayMonth(&month,0x04);
    //displayYear(&year, 0xe0, 0x07);

// ----------------------------------------------------
    
    // Ouverture d'un fichier GPS
    FILE* ubx;
    ubx = fopen("16040118.ubx","rb"); // rb est le mode ==> en lecture binaire
    if(!ubx){
        printf("Probleme de fichier ...");
        return 1;
    }

// ----------------------------------------------------
    // Initialisation Test checksum - Fletcher Algo 
    unsigned char CK_A = 0;
    unsigned char CK_B = 0;

    // Ouverture d'un fichier csv
    FILE* result;
    result = fopen("resultatGPS.csv","w+");
        if(!result){
        printf("Probleme de fichier 2 ...");
        return 1;
    }

    // Ecriture de l entete du fichier
    fprintf(result,"year,month,day,hours,minutes,secondes \n");
	
// ----------------------------------------------------
    // Lecture du fichier GPS et ecriture fichier CSV
    unsigned char id; // Caractère courant
    while(fread(&id,sizeof(char),1,ubx)){ // iterateur qui se deplace, 


	// Verification du message
        if (id == 0xb5){
            fread(&id,sizeof(char),1,ubx);
            if(id == 0x62){
                fread(&id,sizeof(char),1,ubx);
		checkAB(&CK_A, &CK_B, id);
                if(id == 0x01){
                    fread(&id,sizeof(char),1,ubx);
		    checkAB(&CK_A, &CK_B, id);
                    if(id == 0x21){
                        fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);
                        int i=0;
			
			// Passage des termes qui ne representent pas la date
                        while(i!=14){
                            i++;
                            fread(&id,sizeof(char),1,ubx);

			    // Ajout de ces termes a la checksum
			    checkAB(&CK_A, &CK_B, id);
                        }

                        // Gestion année 
                        unsigned char id1 = id;
                        fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);
                        unsigned char id2 = id;
                        displayYear(&year,id1,id2);

                        fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);

                        // Gestion mois 
                        displayMonth(&month,id);

                        fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);

                        // Gestion jour
                        displayDay(&day,id);

                        fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);

                        //Gestion temps
                        unsigned char id3 = id;
                        fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);
                        unsigned char id4 = id;
                        fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);
                        unsigned char id5 = id;

                        displayTime(tim,id3,id4,id5);  
						
			fread(&id,sizeof(char),1,ubx);
			checkAB(&CK_A, &CK_B, id);

			
			// Enregistrement des variables de verification
			fread(&id,sizeof(char),1,ubx);
			unsigned char ck_a = id;
			
			fread(&id,sizeof(char),1,ubx);
			unsigned char ck_b = id;

			// Verification de l algorithme de fletcher
			if(CK_A != ck_a || CK_B != ck_b ){
				printf("Passe pas ...\n");
				
				// SI un message est mauvais on arrete l ecriture dans le fichier
				return 1;
			}

			printf("Algorithme de fletcher valide ==> On ecrit dans le fichier \n");			

                        // Ecriture dans le fichier
                        fprintf(result, "%i,%i,%i,%i,%i,%i \n",year,month,day,tim[0],tim[1],tim[2]);

                        // Visualisation
                        printf("%i,%i,%i,%i,%i,%i \n",year,month,day,tim[0],tim[1],tim[2]);			
                    }
                }
            }
        }  

	// Reinitialisation des verifications pour la prochaine ligne
	CK_A = 0;
	CK_B = 0;
    }

// ----------------------------------------------------
    // Fermeture des ficchiers
    fclose(ubx);
    fclose(result);

    return 0; // Permet de savoir si le code fonctionne .. 1 signifie que non ! 
}
