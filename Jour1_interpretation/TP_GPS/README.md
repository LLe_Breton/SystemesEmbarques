# Décodage d'un message GPS au format binaire


## Table of Contents

1. [Format du message à traiter](#format-du-message-%C3%A0-traiter)
2. [Décodage du message](#d%C3%A9codage-du-message)
3. [Format du message de sortie](#format-du-message-de-sortie)
4. [Vérification de la validité du message](#v%C3%A9rification-de-la-validit%C3%A9-du-message)


## Format du message à traiter
---------------------------------
 
Nous souhaitons traiter un message de type NAV-TIMEUTC, binaire, afin de récupérer l'instant de la mesure GPS.     
Le message se présente comme suit :    
* Le header 
* L'identifiant
* La taille du message 
* Les données
* Les éléments de vérifications

**[Back to top](#table-of-contents)**

## Décodage du message
---------------------------------

Afin de décoder le message binaire, une lecture des données sous forme hexadécimale facilite le traitement.     
Le décodage du message comprend plusieurs étapes :    
* Sélection des messages qui nous interessent : on ne traite que les messages qui ont le header et l'identifiant adaptés à la requête.      
* Récuperation des donnees : on transforme les données qui nous intéressent en données lisibles en base 10.       
* Mise en forme des donnees : on organise les données en colonnes pour plus de lisibilité.     
* Ecriture dans un fichier CSV : on créé un fichier de sortie qui enregistre les données sous un format CSV afin de rendre les données transportables.      


**[Back to top](#table-of-contents)**

## Format du message de sortie
---------------------------------

Le message de sortie est au format CSV afin de permettre une bonne portabilité des données.   
Les données sont enregistrées en colonne selon l'ordre :   
   
`Year, Month, Day, Hour, Minute, Seconde`

L'entête permet de se repérer dans le document final. 


**[Back to top](#table-of-contents)**

## Vérification de la validité du message 
---------------------------------

Avant d'écrire les donénes dans un document, on peut vérifier la validité des données. Pour cela, on utilise la checksum de l'algorithme de fletcher sur 8 bit.   
Cet algorithme vérifie les données sur 2 clés qui sont placées à la fin du message.   
La somme successive de tous les éléments du message ramenée sur 8 bit doit être égale à la première clés. La seconde clé sert de redondance de vérification.
       
Dans ce programme, nous avons choisi d'arrêter le traitement lors d'une erreur de traitement. Il serait également possible de ne pas enregistrer la donnée de la ligne erronnée et de poursuivre le traitement pour les lignes suivantes.
**[Back to top](#table-of-contents)**

## Authors

* **Laure Le Breton**

**[Back to top](#table-of-contents)**
